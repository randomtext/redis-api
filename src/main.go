package main

import (
	"context"
	"fmt"
	"github.com/go-redis/redis/v8"
	"gitlab.com/random-text/redis/src/connections"
	redis2 "gitlab.com/random-text/redis/src/server/delivery/redis"
	"gitlab.com/random-text/redis/src/server/repository"
	"gitlab.com/random-text/redis/src/server/usecase"
	"go.mongodb.org/mongo-driver/mongo"
	"google.golang.org/grpc"
	"os"
)

func initServer(mongoC *mongo.Client, redisC *redis.Client, gc *grpc.ClientConn) {
	repo := repository.New(mongoC)
	useCase := usecase.New(repo, gc)
	_ = redis2.New(useCase, redisC)
}

func initGRPC() *grpc.ClientConn {
	conn, err := connections.GrpcConn()
	if err != nil {
		panic(err)
	}
	return conn
}

func main() {
	mongoC, err := connections.MongoInit()
	if err != nil {
		panic(err)
	}
	redisC := connections.RedisConnect()
	gc := initGRPC()
	defer func() {
		err := mongoC.Disconnect(context.TODO())
		if err != nil {
			panic(err)
		}
		err = redisC.Close()
		if err != nil {
			panic(err)
		}
		err = gc.Close()
		if err != nil {
			panic(err)
		}
	}()

	initServer(mongoC, redisC, gc)
	fmt.Println("redis is running in development mode on port " + os.Getenv("REDIS_PORT"))
	forever := make(chan bool)
	<- forever
}
