package redis

import (
	"context"
	"encoding/json"
	"github.com/go-redis/redis/v8"
	"gitlab.com/random-text/redis/src/payloads"
	"gitlab.com/random-text/redis/src/server"
	"gitlab.com/random-text/redis/src/utils"
)

type handler struct {
	U server.UseCase
	RedisC *redis.Client
}

func New(u server.UseCase, redisC *redis.Client) *handler {
	h := &handler{U: u, RedisC: redisC}
	h.startRedisSubscribe()
	return h
}

func (h *handler) startRedisSubscribe() {
	go func() {
		redisSub := h.RedisC.Subscribe(context.TODO(), "random-texts")
		for {
			msg, err := redisSub.ReceiveMessage(context.TODO())
			if err != nil {
				utils.Log(true, err.Error())
				continue
			}
			var t *payloads.Text
			err = json.Unmarshal([]byte(msg.Payload), &t)
			if err != nil {
				utils.Log(true, err.Error())
				continue
			}
			err = utils.Validate(t, nil)
			if err != nil {
				utils.Log(true, err.Error())
				continue
			}
			h.U.NewText(t)
			utils.Log(false, "NewText")
		}
	}()
}


