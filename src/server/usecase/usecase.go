package usecase

import (
	"context"
	"fmt"
	"gitlab.com/random-text/redis/src/payloads"
	"gitlab.com/random-text/redis/src/server"
	"gitlab.com/random-text/redis/src/server/protos"
	"gitlab.com/random-text/redis/src/utils"
	"go.mongodb.org/mongo-driver/bson"
	"google.golang.org/grpc"
	"time"
)

type usecase struct {
	R server.Repository
	GC *grpc.ClientConn
}

func New(r server.Repository, gc *grpc.ClientConn) server.UseCase {
	return &usecase{R: r, GC: gc}
}

func (u *usecase) NewText(p *payloads.Text) {
	update := bson.M{"redis_status": "Redis ok"}
	err := u.updateTextStatus(p, update)
	if err != nil {
		utils.Log(true, err.Error())
		return
	}
	time.Sleep(time.Second)
	err = u.grpcRequest(p)
	if err != nil {
		utils.Log(true, err.Error())
		update := bson.M{"grpc_status": "GRPC fail"}
		err := u.updateTextStatus(p, update)
		if err != nil {
			utils.Log(true, err.Error())
			return
		}
		return
	}
}

func (u *usecase) updateTextStatus(p *payloads.Text, update bson.M) error {
	filter := bson.M{ "_id": p.ID }
	return u.R.UpdateTextStatus(filter, update)
}

func (u *usecase) grpcRequest(p *payloads.Text) error {
	grpcClient := protos.NewServerClient(u.GC)
	_, err := grpcClient.NewText(context.Background(), &protos.TextPayload{
		Id: p.ID.Hex(),
		Message:  p.Message,
		AuthorId: p.AuthorID,
	})
	fmt.Println("err", err)
	if err != nil {
		return err
	}
	return nil
}
