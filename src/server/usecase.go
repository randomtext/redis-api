package server

import "gitlab.com/random-text/redis/src/payloads"

type UseCase interface {
	NewText(p *payloads.Text)
}
