package connections

import (
	"context"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials"
	"os"
	"time"
)

func GrpcConn() (*grpc.ClientConn, error) {
	if os.Getenv("ENVIRONMENT") == "production" {
		creds, err := credentials.NewClientTLSFromFile(os.Getenv("CERT_FILE"), "")
		if err != nil {
			return nil, err
		}
		ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
		defer cancel()
		conn, err := grpc.DialContext(ctx, os.Getenv("GRPC_URI"), grpc.WithTransportCredentials(creds))
		if err != nil {
			return nil, err
		}
		return conn, nil
	}
	ctx, cancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer cancel()
	conn, err := grpc.DialContext(ctx, os.Getenv("GRPC_URI"), grpc.WithInsecure())
	if err != nil {
		return nil, err
	}
	return conn, nil
}
