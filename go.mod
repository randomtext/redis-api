module gitlab.com/random-text/redis

go 1.16

require (
	github.com/go-playground/locales v0.13.0
	github.com/go-playground/universal-translator v0.17.0
	github.com/go-playground/validator/v10 v10.6.1
	github.com/go-redis/redis/v8 v8.11.0
	go.mongodb.org/mongo-driver v1.5.4
	google.golang.org/grpc v1.39.0
)
